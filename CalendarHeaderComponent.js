import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment';

const weekDaysNames = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
const now = moment();

class CalendarHeaderComponent extends React.PureComponent {
  constructor(props) {
    super(props);
    this.onPressArrowLeft = this.onPressArrowLeft.bind(this);
    this.onPressArrowRight = this.onPressArrowRight.bind(this);
    this.shouldLeftArrowBeDisabled = this.shouldLeftArrowBeDisabled.bind(this);
  }

  onPressArrowLeft() {
    this.props.onPressArrowLeft(this.props.month, this.props.addMonth);
  }

  onPressArrowRight() {
    this.props.onPressArrowRight(this.props.month, this.props.addMonth);
  }

  shouldLeftArrowBeDisabled() {
    const selectedDate = moment(this.props.month.getTime());
    return selectedDate.isSame(now, 'month');
  }

  render() {
    return (
      <View>
        <View style={styles.header}>
          <Text style={styles.dateText}>
            {moment(this.props.month.getTime()).format('MMMM, YYYY')}
          </Text>
          <View style={styles.iconsContainer}>
                <View
                    style={[
                    styles.iconContainer,
                    // this.shouldLeftArrowBeDisabled() ? styles.disabled : {}
                    ]}
                >
                    <TouchableOpacity
                    onPress={this.onPressArrowLeft}
                    // disabled={this.shouldLeftArrowBeDisabled()}
                    >
                    <Image
                        style={[styles.icon, styles.leftIcon]}
                        source={require('./assets/arrow-point-to-right.png')}
                    />
                    </TouchableOpacity>
                </View>
                <TouchableOpacity
                    style={styles.iconContainer}
                    onPress={this.onPressArrowRight}
                >
                    <Image
                    style={styles.icon}
                    source={require('./assets/arrow-point-to-right.png')}
                    />
                </TouchableOpacity>
                </View>
          </View>
          
        {// not showing week day in case of horizontal calendar, this will be handled by day component
        this.props.horizontal ? null : (
          <View style={styles.week}>
            {weekDaysNames.map((day, index) => (
              <Text key={index} style={styles.weekName} numberOfLines={1}>
                {day.toUpperCase()}
              </Text>
            ))}
          </View>
        )}
      </View>
    );
  }
}

CalendarHeaderComponent.propTypes = {
  onPressArrowRight: PropTypes.func.isRequired,
  onPressArrowLeft: PropTypes.func.isRequired,
  addMonth: PropTypes.func,
  month: PropTypes.object
};

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    paddingVertical: 10,
    paddingHorizontal:5,
    backgroundColor: '#fff',
    justifyContent:'space-between'
  },
  week: {
    marginTop: 7,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  weekName: {
    marginTop: 2,
    marginBottom: 7,
    width: 32,
    textAlign: 'center',
    fontSize: 12,
    fontWeight: 'bold',
    color: '#7c7c7c'
  },
  dateText: {
    fontSize: 15,
    color:'#0e6977'
  },
  iconsContainer: {
    flexDirection:'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft:5
  },
  leftIcon: {
    transform: [{ rotate: '180deg' }]
  },
  icon: {
    width: 17,
    height: 17
  },
  disabled: {
    opacity: 0.4
  }
});

export default CalendarHeaderComponent;