import React,  { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
// import { Calendar } from 'react-native-calendars';
import { Calendar } from 'react-native-toggle-calendar';
import CalendarHeaderComponent from './CalendarHeaderComponent'
export default function App() {
  let [currDate, setCurrDate] = useState("2019-11-25")
  onPressArrowLeft = () => {
    setCurrDate("2019-12-25")
  }
  onPressArrowRight = () => {
    setCurrDate("2019-10-25")
  }
  onDayPress = () => {
    
  }
  return (
    <View style={styles.container}>
      <Calendar
        current={currDate}
        calendarHeaderComponent={CalendarHeaderComponent}
        onPressArrowLeft={this.onPressArrowLeft}
        onPressArrowRight={this.onPressArrowRight}
        onDayPress={this.onDayPress}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
